package aste.services;


import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.test.TestHazelcastInstanceFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MessageServiceTest {

	private static TestHazelcastInstanceFactory testInstanceFactory = new TestHazelcastInstanceFactory();

	private IMessageService messageService;

	@Before
	public void setUp() {
		final HazelcastInstance instance = testInstanceFactory.newHazelcastInstance();
		messageService = new MessageServiceImpl(instance);
	}

	@Test
	public void testCount() throws Exception {
		// Given : on met des messages dans la queue.
		this.givenMessages().forEach(messageService::produce);

		// When : on compte les messages dans la queue.
		long count = messageService.count();

		// Then : on a le bon nombre de messages.
		Assert.assertEquals(3, count);
	}

	@After
	public void tearDown() throws Exception {
		Hazelcast.shutdownAll();
	}

	private List<String> givenMessages() {
		List<String> messages = new ArrayList<>();
		messages.add("Message 1");
		messages.add("Message 2");
		messages.add("Message 3");
		return messages;
	}

}
