package aste;

import com.hazelcast.config.Config;
import com.hazelcast.config.ManagementCenterConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfiguration {

	@Bean
	public Config config() {
		Config config = new Config();
		ManagementCenterConfig managementCenterConfig = new ManagementCenterConfig();
		managementCenterConfig.setEnabled(true);
		managementCenterConfig.setUpdateInterval(1);
		managementCenterConfig.setUrl("http://localhost:8080/mancenter");
		config.setManagementCenterConfig(managementCenterConfig);
		return config;
	}
}
