package aste.services;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MessageServiceImpl implements IMessageService {

	public static final String QUEUE = "messages";

	private HazelcastInstance instance;

	@Autowired
	public MessageServiceImpl(HazelcastInstance instance) {
		this.instance = instance;
	}

	@Override
	public long count() {
		IQueue<String> queue = instance.getQueue(QUEUE);
		return queue.size();
	}

	@Override
	public boolean produce(final String message) {
		IQueue<String> queue = instance.getQueue(QUEUE);
		Date date = new Date();
		return queue.offer(message + " - " + date.toString());
	}

	@Override
	public String consume() {
		IQueue<String> queue = instance.getQueue(QUEUE);
		return queue.isEmpty() ? "Empty" : queue.poll();
	}
}
