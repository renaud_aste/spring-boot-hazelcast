package aste.services;

public interface IMessageService {

	/**
	 * On compte les messages dans la file de messages.
	 * @return le nombre de messages dans la file de messages.
	 */
	long count();

	/**
	 * Produit un message dans la file de messages.
	 * @param message le message à ajouter.
	 * @return
	 */
	boolean produce(String message);

	/**
	 * Consomme un message dans la file de message.
	 * @return le message consommé, ou "empty" si la file de messages est vide.
	 */
	String consume();
}
